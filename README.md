<p align="center"> <img src="./Img/cuc.png" width="300"></p>

## Colegio Universitario de Cartago

## TI-162

## Administración y programación de sitios web

## Investigación

## Integrantes

## Victoria Cañas

## Daiber Contreras

## Angel Pereira

## Diego Solano

## 2023


# Primeros pasos

## Clonar el repositorio

## git clone https://taller@gitlab.com/EisenheimDiego/sitios.git 

## token: glpat-qz8cmcTt3stz_QQkLrm6

# Comandos

## git clone "link del repositorio"

## git status

## git push

## git pull

## git init

## git commit -m"Comentario del commit"

## git add . (Para agregar todos los archivos nuevos)

## git add "nombre del archivo" (Para agregar un único archivo)

## git checkout "nombre de la rama"

# Taller

## Considere las siguientes reglas

- Resuelva lo que se le indica **en grupo**
- Utilice la consola de comandos Git Bash para los cambios
- No se utilizarán ramas en el repositorio, por lo que deberá tener una buena comunicación con el otro grupo, para coordinar push y pulls

## Resuelva lo siguiente

- Haga un pull de los últimos cambios del repositorio.
- Solucione, **en grupo** , la parte de código que se le indica.
- Comente, en su commit los cambios realizados.
- Realice un push, para subir sus cambios a el repositorio.

## Grupo #3

### Archivo: ConexionEstudiantes.cs

```csharp
public List<TablaEstudiante> listarEstudiante()
        {
            string query = "select from TablaEstudiante";
            IEnumerable<TablaEstudiante> lista = DB.Database.SqlQuery<TablaEstudiante>(consulta);
            return lista.ToList()
        }
```

- El método listarEstudiante() presenta 3 errores.
- Solucione cada error, para que la funcionalidad del sistema cumpla su propósito.
- Asegúrese de comentar cada línea de código indicando la persona que vió el error.

## Grupo #1

### Archivo: registro.aspx.cs

```csharp
public List<TablaEstudiante> listarEstudiante()
        protected void btnRegistro_Click(object sender, EventArgs e)
        {
            try
            {
                ClientScript.RegisterClientScriptBlock(this.GetType(), "messagebox", "swal('¡Error al registrar los datos!','Los datos ingresados ya han sido registrados','error')", true);   
            }catch(Exception ex)
            {
if (txtCedula.Text == " " || txtNombre.Text == "" || txtPriApellido.Text == " " || txtCorreo.Text == " " )
                {
                    ie.InsertarEstudiante(txtCedula.Text, txtNombre.Text, txtPriApellido.Text, txtTelefono.Text, txtCorreo.Text);
                    ClientScript.RegisterClientScriptBlock(this.GetType(), "messagebox", "swal('¡Los datos fueron validados correctamente!','Registrado','success')", true);}
                else
                {
                    ClientScript.RegisterClientScriptBlock(this.GetType(), "messagebox", "swal('¡Aviso!','No se permiten espacios en blanco!','warning')", true);
                }
            }
        }
```

- El método btnRegistro_Click presenta 3 errores.
- Solucione cada error, para que la funcionalidad del sistema cumpla su propósito.
- Asegúrese de comentar cada línea de código indicando la persona que vió el error.