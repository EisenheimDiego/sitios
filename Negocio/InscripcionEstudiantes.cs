﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Datos;
using Datos.RegistroEstudiantes;

namespace Negocio
{
    
    public class InscripcionEstudiantes
    {

        ConexionEstudiantes conEstu = new ConexionEstudiantes();

        public int InsertarEstudiante(string id, string nom, string ape, string corr, string tel)
        {
            int resultado = conEstu.insertarEstudiante(id, nom, ape, tel,  corr );
            return resultado;
        }

        public List<TablaEstudiante> ConsultarEstudiante()//
        {
            return conEstu.listarEstudiante();

        }



    }
}
