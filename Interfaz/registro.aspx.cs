﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Caching;
using System.Web.Services.Description;
using System.Web.UI;
using System.Web.UI.WebControls;
using Negocio;

namespace Interfaz
{
    public partial class registro : System.Web.UI.Page
    {
        InscripcionEstudiantes ie = new InscripcionEstudiantes();
        protected void Page_Load(object sender, EventArgs e)
        {

        }
        //comentario
        protected void btnRegistro_Click(object sender, EventArgs e)
        {
            try
            {
                ClientScript.RegisterClientScriptBlock(this.GetType(), "messagebox", "swal('¡Error al registrar los datos!','Los datos ingresados ya han sido registrados','error')", true);   
            }catch(Exception ex)
            {
if (txtCedula.Text == " " || txtNombre.Text == "" || txtPriApellido.Text == " " || txtCorreo.Text == " " )
                {
                    ie.InsertarEstudiante(txtCedula.Text, txtNombre.Text, txtPriApellido.Text, txtTelefono.Text, txtCorreo.Text);
                    ClientScript.RegisterClientScriptBlock(this.GetType(), "messagebox", "swal('¡Los datos fueron validados correctamente!','Registrado','success')", true);}
                else
                {
                    ClientScript.RegisterClientScriptBlock(this.GetType(), "messagebox", "swal('¡Aviso!','No se permiten espacios en blanco!','warning')", true);
                }
            }

        }

        protected void btnHome_Click(object sender, EventArgs e)
        {
            Response.Redirect("HomePrincipal.aspx");
        }
    }
}