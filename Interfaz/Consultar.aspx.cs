﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Negocio;
namespace Interfaz
{
    public partial class Consultar : System.Web.UI.Page
    {
        InscripcionEstudiantes inE = new InscripcionEstudiantes();
        protected void Page_Load(object sender, EventArgs e)
        {
            GvConsultar.DataSource = inE.ConsultarEstudiante();
            GvConsultar.DataBind();
        }

        protected void GvConsultar_SelectedIndexChanged(object sender, EventArgs e)
        {

        }
    }
}