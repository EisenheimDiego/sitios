﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="registro.aspx.cs" Inherits="Interfaz.registro" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title></title>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.0.1/sweetalert.min.js" type="text/javascript"></script>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.0.1/sweetalert.css" />
    <meta name="viewport" content="width=device-width, user-scalable=yes, initial-scale=1.0, maximum-scale=3.0, minimum-scale=1.0"/>
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.3/css/all.css" />
    <link rel="stylesheet" href="https://fontawesome.com/icons/id-card?s=solid&f=classic" />
    	<link rel="stylesheet" href="css/estiloslogin.css"/>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.0.1/sweetalert.min.js" type="text/javascript"></script>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.0.1/sweetalert.css" />

    <style type="text/css">
        .auto-style1 {
            border-style: none;
            border-color: inherit;
            border-width: medium;
            width: 15%;
            color: white;
            font-size: 20px;
            background: #1a2537;
            padding: 15px 20px;
            border-radius: 5px;
            cursor: pointer;
            margin-left: 200px;
        }
        .buttonHome:hover {
            background: cadetblue;
        }
    </style>
</head>
<body background="img/fondo.jpg">
    <form id="form1" runat="server" class="formulario">
        <h1>Registrate</h1>
     <div class="contenedor">

         <div class="input-contenedor">         
             <i class='fas fa-id-card icon'></i>
             <asp:TextBox ID="txtCedula" runat="server" placeholder="Cédula" ></asp:TextBox>      
         </div>
     
        <div class="input-contenedor">
             <i class="fas fa-user icon"></i>
            <asp:TextBox ID="txtNombre" runat="server" placeholder="Nombre" ></asp:TextBox>      
         </div>

         <div class="input-contenedor">
             <i class="fas fa-user icon"></i>
             <asp:TextBox ID="txtPriApellido" runat="server" placeholder="Primer Apellido" ></asp:TextBox>              
         </div>

         

        <div class="input-contenedor">
        <i class='fas fa-envelope icon'></i>
        <asp:TextBox ID="txtCorreo" runat="server" placeholder="Correo Electrónico" ></asp:TextBox>
         </div>
         
         <div class="input-contenedor">
         <i class='fas fa-phone icon'></i>
         <asp:TextBox ID="txtTelefono" runat="server" placeholder="Teléfono" ></asp:TextBox>
         </div>
         
         

         <asp:Button ID="btnRegistro" runat="server" Text="Registrate"  CssClass="button" OnClick="btnRegistro_Click" />
         <p>Al registrarte, aceptas nuestras Condiciones de uso y Política de privacidad.</p>
      
         <asp:Button ID="btnHome" runat="server" Text="Home"  CssClass="button" OnClick="btnHome_Click" />
     </div>
    </form>
</body>
</html>
